package com.zuitt.example;

import java.util.Scanner;

public class S1A1 {
    public static void main(String[] args) {

        Scanner myObj = new Scanner(System.in);

        String firstname;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;

        String subjectInput;


        System.out.print("First Name: ");
        firstname = myObj.nextLine();

        System.out.print("Last Name: ");
        lastName = myObj.nextLine();

        System.out.print("First Subject Grade: ");
        subjectInput = myObj.nextLine();
        firstSubject = Double.parseDouble(subjectInput);

        System.out.print("Second Subject Grade: ");
        subjectInput = myObj.nextLine();
        secondSubject = Double.parseDouble(subjectInput);


        System.out.print("Third Subject Grade: ");
        subjectInput = myObj.nextLine();
        thirdSubject = Double.parseDouble(subjectInput);

        double average = (firstSubject+secondSubject+thirdSubject)/3;

        System.out.println("Good day "+firstname+" "+lastName+",");
        System.out.println("Your grade is: "+average);




    }
}
